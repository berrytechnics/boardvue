function BuildiP6(){
	// var img = document.getElementById('mobo6');
	// c.drawImage(img,0,0);
	// c.rotate(-10*Math.PI/180);
	// c.transform(1,0,0,0,0,0);
	c.beginPath();
	c.moveTo(46,242);
	c.lineTo(90,242);
	c.lineTo(90,235);
	c.lineTo(507,235);
	c.lineTo(507,34);
	c.lineTo(538,34);
	c.lineTo(550,21);
	c.lineTo(570,21);
	c.arc(575,31,10,Math.PI*1.5,Math.PI*.5-1.5);
	c.lineTo(587,92);
	c.lineTo(531,92);
	c.lineTo(531,127);
	c.lineTo(526,132);
	c.lineTo(526,197);
	c.lineTo(585,197);
	c.lineTo(585,214);
	c.lineTo(539,214);
	c.lineTo(539,240);
	c.lineTo(532,240);
	c.lineTo(532,262);
	c.lineTo(539,262);
	c.lineTo(539,310);
	c.arc(535,320,9,Math.PI*1.5+1,Math.PI*.5);
	c.lineTo(513,329);
	c.arc(503,333,10,0,2.75,false);
	c.lineTo(288,338);
	c.lineTo(288,332);
	c.lineTo(226,332);
	c.lineTo(226,338);
	c.lineTo(46,338);
	c.lineTo(46,242);
	c.strokeStyle = 'black';
	c.stroke();
	//screw holes
	c.beginPath();
	c.arc(56,253,7,0,Math.PI*2);
	c.stroke();
	c.beginPath();
	c.arc(82,330,5,0,Math.PI*2);
	c.stroke();
	c.beginPath();
	c.arc(215,329,7,0,Math.PI*2);
	c.stroke();
	c.beginPath();
	c.arc(503,333,7,0,Math.PI*2);
	c.stroke();
	c.beginPath();
	c.arc(528,207,7,0,Math.PI*2);
	c.stroke();
	c.beginPath();
	c.arc(526,81,7,0,Math.PI*2);
	c.stroke();
	c.beginPath();
	c.arc(575.5,64.5,7,0,Math.PI*2);
	c.stroke();
	c.beginPath();
	c.arc(554.5,33,7,0,Math.PI*2);
	c.stroke();
	c.beginPath();
	c.arc(517,43,7,0,Math.PI*2);
	c.stroke();
	c.beginPath();
	c.arc(571,33,5,0,Math.PI*2);
	c.lineWidth=5;
	c.stroke();
	c.beginPath();
	c.arc(577.7,81.5,5,0,Math.PI*2);
	c.lineWidth=5;
	c.stroke();
	c.beginPath();
	c.arc(570,205.5,5,0,Math.PI*2);
	c.lineWidth=2;
	c.stroke();
	c.beginPath();
	c.arc(535,319,4,0,Math.PI*2);
	c.lineWidth=5;
	c.stroke();
	c.beginPath();
	c.arc(471,298.5,4,0,Math.PI*2);
	c.lineWidth=5;
	c.stroke();
	c.beginPath();
	c.arc(437,326,4,0,Math.PI*2);
	c.lineWidth=5;
	c.stroke();
	c.beginPath();
	c.arc(432.5,245,4,0,Math.PI*2);
	c.lineWidth=5;
	c.stroke();
}
var ic = 
{
	"1": {
		"name": "U2",
		"grid": 36,
		"radius":4,
		"chip": {
			"x": 100,
			"y": 100,
			"h": 60,
			"w": 60,
			"color":"black"
		},
		"1": {
			"net":"pp1v8",
			"x":105,
			"y":105,
			"color":"red"
		},
		"2": {
			"net":"pp1v8",
			"x":115,
			"y":105,
			"color":"red"
		},
		"3": {
			"net":"pp3v0",
			"x":125,
			"y":105,
			"color":"red"
		},
		"4": {
			"net":"pp3v0",
			"x":135,
			"y":105,
			"color":"red"
		},
		"5": {
			"net":"pp1v8",
			"x":145,
			"y":105,
			"color":"red"
		},
		"6": {
			"net":"",
			"x":155,
			"y":105,
			"color":"red"
		},
		"7": {
			"net":"",
			"x":105,
			"y":115,
			"color":"red"
		},
		"8": {
			"net":"pp3v0",
			"x":115,
			"y":115,
			"color":"red"
		},
		"9": {
			"net":"",
			"x":125,
			"y":115,
			"color":"red"
		},
		"10": {
			"net":"pp1v8",
			"x":135,
			"y":115,
			"color":"red"
		},
		"11": {
			"net":"",
			"x":145,
			"y":115,
			"color":"red"
		},
		"12": {
			"net":"pp3v0",
			"x":155,
			"y":115,
			"color":"red"
		},
		"13": {
			"net":"",
			"x":105,
			"y":125,
			"color":"red"
		},
		"14": {
			"net":"",
			"x":115,
			"y":125,
			"color":"red"
		},
		"15": {
			"net":"",
			"x":125,
			"y":125,
			"color":"red"
		},
		"16": {
			"net":"",
			"x":135,
			"y":125,
			"color":"red"
		},
		"17": {
			"net":"",
			"x":145,
			"y":125,
			"color":"red"
		},
		"18": {
			"net":"",
			"x":155,
			"y":125,
			"color":"red"
		},
		"19": {
			"net":"pp1v8",
			"x":105,
			"y":135,
			"color":"red"
		},
		"20": {
			"net":"",
			"x":115,
			"y":135,
			"color":"red"
		},
		"21": {
			"net":"",
			"x":125,
			"y":135,
			"color":"red"
		},
		"22": {
			"net":"",
			"x":135,
			"y":135,
			"color":"red"
		},
		"23": {
			"net":"",
			"x":145,
			"y":135,
			"color":"red"
		},
		"24": {
			"net":"",
			"x":155,
			"y":135,
			"color":"red"
		},
		"25": {
			"net":"",
			"x":105,
			"y":145,
			"color":"red"
		},
		"26": {
			"net":"",
			"x":115,
			"y":145,
			"color":"red"
		},
		"27": {
			"net":"",
			"x":125,
			"y":145,
			"color":"red"
		},
		"28": {
			"net":"",
			"x":135,
			"y":145,
			"color":"red"
		},
		"29": {
			"net":"",
			"x":145,
			"y":145,
			"color":"red"
		},
		"30": {
			"net":"pp1v8",
			"x":155,
			"y":145,
			"color":"red"
		},
		"31": {
			"net":"",
			"x":105,
			"y":155,
			"color":"red"
		},
		"32": {
			"net":"",
			"x":115,
			"y":155,
			"color":"red"
		},
		"33": {
			"net":"",
			"x":125,
			"y":155,
			"color":"red"
		},
		"34": {
			"net":"",
			"x":135,
			"y":155,
			"color":"red"
		},
		"35": {
			"net":"",
			"x":145,
			"y":155,
			"color":"red"
		},
		"36": {
			"net":"pp1v8",
			"x":155,
			"y":155,
			"color":"red"
		},

	},
    "2": {
		  "name": "U1401",
				"grid": 2,
				"radius":4,
				"chip": {
					"x": 400,
					"y": 100,
					"h": 60,
					"w": 60,
					"color":"black"
				},
				"1": {
					"net":"pp1v8",
					"x":405,
					"y":105,
					"color":"red"
				},
				"2": {
					"net":"pp3v0",
					"x":415,
					"y":105,
					"color":"red"
				}
    },
    "3":{
    	"name":"LDOreg",
    	"grid":4,
    	"radius":1.75,
    	"chip":{
    		"x":200,
    		"y":300,
    		"w":10,
    		"h":10,
    		"color":"black"
    	},
    	"1":{
    		"net":"pp1v8",
    		"x":203,
    		"y":303,
    		"color":"red"
    	},
    	"2":{
    		"net":"pp3v0",
    		"x":207,
    		"y":303,
    		"color":"red"
    	},
    	"3":{
    		"net":"gnd",
    		"x":203,
    		"y":307,
    		"color":"red"
    	},
    	"4":{
    		"net":"gnd",
    		"x":207,
    		"y":307,
    		"color":"red"
    	}
    }
}