# BoardVue

### BoardVue is an in-progress HTML5 boardview software for iPhone motherboards.
#### https://berrytechnics.github.io/BoardVue/

The current goal is to use an HTML canvas with JSON (or Js nested objects) combined with loops to highlight circuit NETS onmouseover.

Individual component data is stored in dimensional object in assets.js

Mouse coordinate and NET info is stored in nets.js
