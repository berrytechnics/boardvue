//setup canvas
var canvas = document.getElementById("canvas");
var c = canvas.getContext("2d");
canvas.width = innerWidth;
canvas.height = innerHeight*.8;
//global variables
var boardParts = Object.keys(ic).length;
var mousex = 0;
var mousey = 0;
//build function
function BuildIC(Net){
	for(i=1;i<=boardParts;i++){
	    c.clearRect(ic[i].chip.x,ic[i].chip.y,ic[i].chip.w,ic[i].chip.h);
		c.beginPath();
		c.stroke();
		c.fillStyle = ic[i].chip.color;
		c.fillRect(ic[i].chip.x,ic[i].chip.y,ic[i].chip.w,ic[i].chip.h);
		//
		for(j=1;j<=ic[i].grid;j++){
			ic[i][j].color = 'red';
			if(ic[i][j].net==Net){         
				ic[i][j].color = 'yellow'  
			}                              
			c.beginPath();
			c.fillStyle = ic[i][j].color;
			var arc = c.arc(ic[i][j].x,ic[i][j].y,ic[i].radius,0,Math.PI*2);
			c.fill();
			document.getElementById('status').innerHTML = Net || "No Net";

		}
	}
}
//Mouse COORD tracker
canvas.addEventListener('mousemove',netSelect,false);
function netSelect(event){
	mousex = event.x;
	mousey = event.y;
	mousex -= canvas.offsetLeft;
	mousey -= canvas.offsetTop;
    Nets()
}
BuildiP6();
BuildIC();
// function Zoom(percent){
// 	for(i=1;i<=boardParts;i++){
// 		for(j=1;j<=ic[i].grid;j++){
//             ic[i][j].x += 50
//             BuildIC()
// 		}
// 	}
// };
// Zoom();

